var express = require("express");
var login = require('../FileIO/LoginIO.js');
var hash = require('hash.js');
var router = express.Router();

router.get('/', function(req,res){
    var api_key = hash.sha1().update( new Date().toISOString() + '.' + req.query.user).digest('hex');
    login.createUserJson(req.query.user, req.query.pass, req.query.email,hash.sha224().update(api_key).digest('hex'), res);
})





module.exports = router;